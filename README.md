# DEPRECATED, please use [eslint-config-codeyellow](https://www.npmjs.com/package/eslint-config-codeyellow)

# codeyellow-style

This repository contains configuration files for linters.

## Default editor settings
[EditorConfig](http://editorconfig.org/) ensures we all use the same line endings, spaces and such. Check it out in `.editorconfig`.

## Javascript
For Javascript [ESLint](http://eslint.org/) is used. ESLint is a bit like JSHint and JSCS combined. It checks code quality *and* code style. It is also suited for ES6. Check it out in `.eslintrc`. Make sure you use version `1.0.0` or higher.

The styleguide is based on [Airbnb](https://github.com/airbnb/javascript), with a few changes.